#include <helpers/files>
#include <helpers/arrays>
#include <helpers/grouping>

public void OnPluginStart()
{
	ArrayList inputs = LoadIntInputsFromFile("addons/sourcemod/data/aoc-inputs/day4.in", "-");

	int lowerRange = inputs.Get(0);
	int upperRange = inputs.Get(1);

	int part1 = GetValidCandicates(lowerRange, upperRange, true);
	int part2 = GetValidCandicates(lowerRange, upperRange, false);

	PrintToServer("Part 1: %d", part1);
	PrintToServer("Part 2: %d", part2);
}

int GetValidCandicates(int lowerRange, int upperRange, bool doPartOne)
{
	int validCandicates = 0;
	for (int i = lowerRange; i < upperRange; i++)
	{
		char rangeStr[7]; // 6 + 1 (null term!)
		IntToString(i, rangeStr, sizeof(rangeStr));

		ArrayList candicate = new ArrayList();
		for (int j = 0; j < 6; j++)
		{
			candicate.Push(rangeStr[j]);
		}

		if (IsValidCandicate(candicate, doPartOne))
		{
			validCandicates++;
		}

		delete candicate;
	}

	return validCandicates;
}

bool IsValidCandicate(ArrayList candicate, bool doPartOne)
{
	if (candicate.Length != 6)
	{
		return false;
	}

	ArrayList ordered = candicate.Clone();
	ordered.Sort(Sort_Ascending, Sort_Integer);

	if (!SequenceEquals(candicate, ordered))
	{
		// Increasing digits failed...
		delete ordered;
		return false;
	}

	ArrayList groups = GroupIntegers(candicate);

	int adjacentDigits = 0;
	bool twoAdjacentDigits = false;

	for (int i = 0; i < groups.Length; i++)
	{
		ArrayList group = groups.Get(i);
		if (group.Length == 2)
		{
			twoAdjacentDigits = true;
		}

		if (adjacentDigits < group.Length)
		{
			adjacentDigits = group.Length;
		}
	}

	delete groups;
	delete ordered;

	if (doPartOne)
	{
		return (adjacentDigits >= 2);
	}

	return twoAdjacentDigits; // we dont really care, as long as we get our two adjacent ones...
}