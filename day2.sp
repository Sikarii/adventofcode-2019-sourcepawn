#include <helpers/files>

public void OnPluginStart()
{
	ArrayList inputs = LoadIntInputsFromFile("addons/sourcemod/data/aoc-inputs/day2.in", ",");

	inputs.Set(1, 12); // inputs[1] = 12;
	inputs.Set(2, 2); // inputs[2] = 2;

	int part1 = RunIntCode(inputs.Clone());
	int part2 = SolvePart2(inputs);
	
	PrintToServer("Part 1: %d", part1);
	PrintToServer("Part 2: %d", part2);
}

int SolvePart2(ArrayList inputs)
{
	for (int noun = 0; noun < 100; noun++)
	{
		for (int verb = 0; verb < 100; verb++)
		{
			ArrayList numbers = inputs.Clone();

			numbers.Set(1, noun);
			numbers.Set(2, verb);

			int output = RunIntCode(numbers);
			if (output != 19690720)
			{
				continue;
			}

			return (100 * noun + verb);
		}
	}

	return -1; // Failed to solve...
}

int RunIntCode(ArrayList numbers)
{
	int cursor = 0;
	while (numbers.Get(cursor) != 99)
	{
		int opCode = numbers.Get(cursor);

		int addr1 = numbers.Get(cursor + 1);
		int addr2 = numbers.Get(cursor + 2);
		int addr3 = numbers.Get(cursor + 3);

		int val1 = numbers.Get(addr1);
		int val2 = numbers.Get(addr2);

		int result = 0;
		switch (opCode)
		{
			case 1: result = (val1 + val2);
			case 2: result = (val1 * val2);
			default: SetFailState("Invalid opcode passed!");
		}

		numbers.Set(addr3, result);
		cursor += 4;
	}

	return numbers.Get(0);
}