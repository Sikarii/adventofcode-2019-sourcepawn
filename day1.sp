#include <helpers/files>

public void OnPluginStart()
{
	ArrayList inputs = LoadIntInputsFromFile("addons/sourcemod/data/aoc-inputs/day1.in");
	
	int part1 = CalculateTotalFuel(inputs);
	int part2 = CalculateTotalFuelMass(inputs);

	PrintToServer("Part 1: %d", part1);
	PrintToServer("Part 2: %d", part2);
}

int CalculateFuel(int value)
{
	return (value / 3) - 2;
}

int CalculateFuelMass(int value)
{
	int fuel = CalculateFuel(value);
	if (fuel <= 0)
	{
		return 0;
	}

	return (fuel + CalculateFuelMass(fuel));
}

int CalculateTotalFuel(ArrayList values)
{
	int sum = 0;
	for (int i = 0; i < values.Length; i++)
	{
		int value = values.Get(i);
		sum += CalculateFuel(value);
	}

	return sum;
}

int CalculateTotalFuelMass(ArrayList values)
{
	int sum = 0;
	for (int i = 0; i < values.Length; i++)
	{
		int value = values.Get(i);
		sum += CalculateFuelMass(value);
	}

	return sum;
}