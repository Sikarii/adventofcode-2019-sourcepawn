#if defined _AOCGroupingHelper_included_
	#endinput
#endif

#define _AOCGroupingHelper_included_

// NOTE: All children (groups) will be leaked with the traditional "delete" on the main ArrayList!
// But we don't care as these challenges are not meant for a production use case...
stock ArrayList GroupIntegers(ArrayList values)
{
	ArrayList groups = new ArrayList();

	for (int i = 0; i < values.Length; i++)
	{
		int value = values.Get(i);
		ArrayList group = FindGroupWithInt(groups, value);

		if (group == null)
		{
			group = new ArrayList();
			groups.Push(group);
		}

		group.Push(value);
	}

	return groups;
}

static ArrayList FindGroupWithInt(ArrayList groups, int value)
{
	for (int i = 0; i < groups.Length; i++)
	{
		ArrayList group = groups.Get(i);
		for (int j = 0; j < group.Length; j++)
		{
			int groupValue = group.Get(j);
			if (value == groupValue)
			{
				return group;
			}
		}
	}

	return null;
}