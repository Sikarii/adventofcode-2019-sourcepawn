#if defined _AOCFilesHelper_included_
	#endinput
#endif

#define _AOCFilesHelper_included_

#define MAX_INPUT_ITEM 64
#define MAX_INPUT_BUFFER 102400

static char FileBuffer[MAX_INPUT_BUFFER];

stock ArrayList LoadInputsFromFile(const char[] filePath, const char[] delimiter = "\n")
{
	FileBuffer[0] = '\0';
	ArrayList output = new ArrayList(ByteCountToCells(MAX_INPUT_ITEM));

	File file = OpenFile(filePath, "r");
	file.ReadString(FileBuffer, sizeof(FileBuffer));

	delete file;

	char split[MAX_INPUT_BUFFER / MAX_INPUT_ITEM][MAX_INPUT_ITEM];
	int splitCount = ExplodeString(FileBuffer, delimiter, split, sizeof(split), sizeof(split[]));

	for (int i = 0; i < splitCount; i++)
	{
		output.PushString(split[i]);
	}

	return output;
}

stock ArrayList LoadIntInputsFromFile(const char[] filePath, const char[] delimiter = "\n")
{
	ArrayList output = new ArrayList(); // We only store cells, dont give blocksize
	ArrayList inputs = LoadInputsFromFile(filePath, delimiter);

	for (int i = 0; i < inputs.Length; i++)
	{
		// int32 can only be of 11 size, no reason to get more...
		char item[12];
		inputs.GetString(i, item, sizeof(item));

		int itemValue = StringToInt(item);
		output.Push(itemValue);
	}

	delete inputs;
	return output;
}