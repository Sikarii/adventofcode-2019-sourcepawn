#if defined _AOCBuffersHelper_included_
	#endinput
#endif

#define _AOCBuffersHelper_included_

stock ArrayList MakeIntArrayList(int[] ints, int intsLen)
{
	ArrayList output = new ArrayList();
	for (int i = 0; i < intsLen; i++)
	{
		int value = ints[i];
		output.Push(value);
	}

	return output;
}

stock bool SequenceEquals(ArrayList one, ArrayList two)
{
	if (one.Length != two.Length)
	{
		return false;
	}

	for (int i = 0; i < one.Length; i++)
	{
		int val1 = one.Get(i);
		int val2 = two.Get(i);

		if (val1 != val2)
		{
			return false;
		}
	}

	return true;
}
